package com.footsqueek.android.hearingvoices;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.footsqueek.android.hearingvoices.PodcastsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.footsqueek.android.hearingvoices.PodcastsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PodcastsFragment extends Fragment {

    private PodcastsGridViewAdapter podcastsGridViewAdapter;
    private GridView podcastsGridView;
    private RelativeLayout disclaimerLayout, simulationsLayout;
    private ImageView peterPodcastButton, katePodcastButton, bobPodcastButton, shaunPodcastButton;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static PodcastsFragment newInstance() {
        PodcastsFragment fragment = new PodcastsFragment();
        return fragment;
    }

    public PodcastsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_podcasts, container, false);

        Cursor cursor = null;

        // Create a MatrixCursor filled with the rows you want to add.
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{"_id", "title", "summary", "action", "url"});
        matrixCursor.addRow(new Object[]{"1", "Activity 1", "1", 1, ""});
        matrixCursor.addRow(new Object[]{"2", "Activity 2", "2", 2, ""});
        matrixCursor.addRow(new Object[]{"3", "Activity 3", "3", 0, ""});
        matrixCursor.addRow(new Object[]{"4", "Exercise 1 - 5 minutes minimum", "1", 0, ""});
        matrixCursor.addRow(new Object[]{"5", "Exercise 2 - 15 - 30 minutes minimum", "2", 0, ""});
        matrixCursor.addRow(new Object[]{"6", "Exercise 3 - over an hour", "3", 0, ""});

        // Merge your existing cursor with the matrixCursor you created.
        MergeCursor mergeCursor = new MergeCursor(new Cursor[]{matrixCursor, cursor});

        podcastsGridViewAdapter = new PodcastsGridViewAdapter(getActivity(), mergeCursor, true);

//        podcastsGridView = (GridView) v.findViewById(R.id.podcastsGridView);
//        podcastsGridView.setAdapter(podcastsGridViewAdapter);

        peterPodcastButton = (ImageView)v.findViewById(R.id.peterPodcastButton);
        peterPodcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPodcast(1);
            }
        });

        katePodcastButton = (ImageView)v.findViewById(R.id.katePodcastButton);
        katePodcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPodcast(2);
            }
        });

        bobPodcastButton = (ImageView)v.findViewById(R.id.bobPodcastButton);
        bobPodcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPodcast(3);
            }
        });

        shaunPodcastButton = (ImageView)v.findViewById(R.id.shaunPodcastButton);
        shaunPodcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPodcast(4);
            }
        });

        return v;
    }

    private void playPodcast (int tag) {

//        Uri videoUri = null;
//
//        switch (tag) {
//            case 1:
//                videoUri = Uri.parse("android.resource://" + getActivity().getApplicationContext().getPackageName() + "/" + R.raw.peterstart);
//                break;
//            case 2:
//                videoUri = Uri.parse("android.resource://" + getActivity().getApplicationContext().getPackageName() + "/" + R.raw.katecoping);
//                break;
//            case 3:
//                videoUri = Uri.parse("android.resource://" + getActivity().getApplicationContext().getPackageName() + "/" + R.raw.bobcoping);
//                break;
//            case 4:
//                videoUri = Uri.parse("android.resource://" + getActivity().getApplicationContext().getPackageName() + "/" + R.raw.bobcoping);
//                break;
//        }
//
//        Intent intent = new Intent(getActivity(), VideoActivity.class);
//        intent.putExtra("videoUri", videoUri.toString());
//        startActivityForResult(intent, 0);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}