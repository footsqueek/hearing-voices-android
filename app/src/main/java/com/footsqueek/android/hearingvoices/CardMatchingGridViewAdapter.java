package com.footsqueek.android.hearingvoices;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by wmaginn on 17/01/2015.
 */
public class CardMatchingGridViewAdapter extends CursorAdapter {

    public CardMatchingGridViewAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public CardMatchingGridViewAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View playingCardItemView = inflater.inflate(R.layout.playing_card_list_item, parent, false);

        return playingCardItemView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView simulationTitleTextView = (TextView)view.findViewById(R.id.simulationTitleTextView);
        String simulationTitle = cursor.getString(cursor.getColumnIndex("suit"));
        simulationTitleTextView.setText(simulationTitle);

        TextView simulationSummaryTextView = (TextView)view.findViewById(R.id.simulationSummaryTextView);
        String simulationSummary = cursor.getString(cursor.getColumnIndex("value"));
        simulationSummaryTextView.setText(simulationSummary);

        ImageView playingCardImageView = (ImageView)view.findViewById(R.id.playingCardImageView);

        if (simulationTitle.equals("Clubs")) {
            switch (Integer.valueOf(simulationSummary)) {
                case 1: {
                    playingCardImageView.setBackgroundResource(R.drawable.c1);
                }
                break;
                case 2: {
                    playingCardImageView.setBackgroundResource(R.drawable.c2);
                }
                break;
                case 3: {
                    playingCardImageView.setBackgroundResource(R.drawable.c3);
                }
                break;
                case 4: {
                    playingCardImageView.setBackgroundResource(R.drawable.c4);
                }
                break;
                case 5: {
                    playingCardImageView.setBackgroundResource(R.drawable.c5);
                }
                break;
                case 6: {
                    playingCardImageView.setBackgroundResource(R.drawable.c6);
                }
                break;
                case 7: {
                    playingCardImageView.setBackgroundResource(R.drawable.c7);
                }
                break;
                case 8: {
                    playingCardImageView.setBackgroundResource(R.drawable.c8);
                }
                break;
                case 9: {
                    playingCardImageView.setBackgroundResource(R.drawable.c9);
                }
                break;
                case 10: {
                    playingCardImageView.setBackgroundResource(R.drawable.c10);
                }
                break;
            }
        } else if (simulationTitle.equals("Diamonds")) {
            switch (Integer.valueOf(simulationSummary)) {
                case 1: {
                    playingCardImageView.setBackgroundResource(R.drawable.d1);
                }
                break;
                case 2: {
                    playingCardImageView.setBackgroundResource(R.drawable.d2);
                }
                break;
                case 3: {
                    playingCardImageView.setBackgroundResource(R.drawable.d3);
                }
                break;
                case 4: {
                    playingCardImageView.setBackgroundResource(R.drawable.d4);
                }
                break;
                case 5: {
                    playingCardImageView.setBackgroundResource(R.drawable.d5);
                }
                break;
                case 6: {
                    playingCardImageView.setBackgroundResource(R.drawable.d6);
                }
                break;
                case 7: {
                    playingCardImageView.setBackgroundResource(R.drawable.d7);
                }
                break;
                case 8: {
                    playingCardImageView.setBackgroundResource(R.drawable.d8);
                }
                break;
                case 9: {
                    playingCardImageView.setBackgroundResource(R.drawable.d9);
                }
                break;
                case 10: {
                    playingCardImageView.setBackgroundResource(R.drawable.d10);
                }
                break;
            }
        } else if (simulationTitle.equals("Hearts")) {
            switch (Integer.valueOf(simulationSummary)) {
                case 1: {
                    playingCardImageView.setBackgroundResource(R.drawable.h1);
                }
                break;
                case 2: {
                    playingCardImageView.setBackgroundResource(R.drawable.h2);
                }
                break;
                case 3: {
                    playingCardImageView.setBackgroundResource(R.drawable.h3);
                }
                break;
                case 4: {
                    playingCardImageView.setBackgroundResource(R.drawable.h4);
                }
                break;
                case 5: {
                    playingCardImageView.setBackgroundResource(R.drawable.h5);
                }
                break;
                case 6: {
                    playingCardImageView.setBackgroundResource(R.drawable.h6);
                }
                break;
                case 7: {
                    playingCardImageView.setBackgroundResource(R.drawable.h7);
                }
                break;
                case 8: {
                    playingCardImageView.setBackgroundResource(R.drawable.h8);
                }
                break;
                case 9: {
                    playingCardImageView.setBackgroundResource(R.drawable.h9);
                }
                break;
                case 10: {
                    playingCardImageView.setBackgroundResource(R.drawable.h10);
                }
                break;
            }
        } else if (simulationTitle.equals("Spades")) {
            switch (Integer.valueOf(simulationSummary)) {
                case 1: {
                    playingCardImageView.setBackgroundResource(R.drawable.s1);
                }
                break;
                case 2: {
                    playingCardImageView.setBackgroundResource(R.drawable.s2);
                }
                break;
                case 3: {
                    playingCardImageView.setBackgroundResource(R.drawable.s3);
                }
                break;
                case 4: {
                    playingCardImageView.setBackgroundResource(R.drawable.s4);
                }
                break;
                case 5: {
                    playingCardImageView.setBackgroundResource(R.drawable.s5);
                }
                break;
                case 6: {
                    playingCardImageView.setBackgroundResource(R.drawable.s6);
                }
                break;
                case 7: {
                    playingCardImageView.setBackgroundResource(R.drawable.s7);
                }
                break;
                case 8: {
                    playingCardImageView.setBackgroundResource(R.drawable.s8);
                }
                break;
                case 9: {
                    playingCardImageView.setBackgroundResource(R.drawable.s9);
                }
                break;
                case 10: {
                    playingCardImageView.setBackgroundResource(R.drawable.s10);
                }
                break;
            }
        }

    }
}
