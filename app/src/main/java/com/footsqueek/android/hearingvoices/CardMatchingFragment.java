package com.footsqueek.android.hearingvoices;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CardMatchingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CardMatchingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CardMatchingFragment extends Fragment {

    private TextView scoreTextView, timeTextView;
    private GridView cardsGridView;
    private CountDownTimer countDownTimer;
    private CardMatchingGridViewAdapter cardMatchingGridViewAdapter;
    private ArrayList<String> cardsSelectedArray = new ArrayList<String>();
    private ArrayList<String> cardsUUIDListArray = new ArrayList<String>();
    private ArrayList<PlayingCard> cardArrayList = new ArrayList<PlayingCard>();
    private MediaPlayer mp;

    private OnFragmentInteractionListener mListener;

    // TODO: Rename and change types and number of parameters
    public static CardMatchingFragment newInstance(String param1, String param2) {
        CardMatchingFragment fragment = new CardMatchingFragment();
        return fragment;
    }

    public CardMatchingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_card_matching, container, false);

        scoreTextView = (TextView)v.findViewById(R.id.scoreTextView);
        timeTextView = (TextView)v.findViewById(R.id.timeTextView);

        Cursor cursor = null;

        // Create a MatrixCursor filled with the rows you want to add.
        MatrixCursor matrixCursor = new MatrixCursor(new String[] { "_id", "suit", "value"});

        int cardToDuplicate = HearingVoicesApplication.randomIntInRange(0, 5);
        int placementOfDuplicate = HearingVoicesApplication.randomIntInRange(0, 5);
        if (cardToDuplicate == placementOfDuplicate) {
            while (placementOfDuplicate == cardToDuplicate) {
                placementOfDuplicate = HearingVoicesApplication.randomIntInRange(0, 5);
            }
        }

        for (int i = 0; i < 6; i++) {
            PlayingCard playingCard = new PlayingCard();
            while (cardArrayList.contains(playingCard)) {
                playingCard = new PlayingCard();
            }
            cardArrayList.add(playingCard);
            cardsUUIDListArray.add(playingCard.uuid);
        }

        cardArrayList.set(placementOfDuplicate, cardArrayList.get(cardToDuplicate));

        for (int i = 0; i < cardArrayList.size(); i++) {
            PlayingCard playingCard = cardArrayList.get(i);
            matrixCursor.addRow(new Object[]{i, playingCard.suit, playingCard.value});
        }

        MergeCursor mergeCursor = new MergeCursor(new Cursor[] { matrixCursor, cursor });

        cardMatchingGridViewAdapter = new CardMatchingGridViewAdapter(getActivity(), mergeCursor, true);

        cardsGridView = (GridView)v.findViewById(R.id.cardsGridView);
        cardsGridView.setAdapter(cardMatchingGridViewAdapter);
        cardsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (cardsSelectedArray.contains(cardsUUIDListArray.get(position))) {
                    view.setBackgroundColor(Color.TRANSPARENT);
                    cardsSelectedArray.remove(cardsUUIDListArray.get(position));
                } else {
//                    if (cardsSelectedArray.size() == 2) {
//                        String lastSelectedCard = cardsSelectedArray.get(cardsSelectedArray.size()-1);
//                        Log.i(HearingVoicesApplication.TAG, "UUID: " + lastSelectedCard + " INDEX: " + cardsUUIDListArray.indexOf(lastSelectedCard));
//                        cardsGridView.getChildAt(cardsUUIDListArray.indexOf(lastSelectedCard)).setBackgroundColor(Color.TRANSPARENT);
//                        cardsSelectedArray.remove(lastSelectedCard);
//                    }
                    view.setBackgroundColor(Color.LTGRAY);
                    cardsSelectedArray.add(cardsUUIDListArray.get(position));
                }
                checkPlayingCardsMatch();
            }
        });

        countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeTextView.setText("Time: " + millisUntilFinished/1000);
            }

            @Override
            public void onFinish() {
                mp.release();
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        };
        countDownTimer.start();

        mp = new MediaPlayer();
        Uri myUri = Uri.parse("android.resource://" + getActivity().getApplicationContext().getPackageName() + "/" + R.raw.activityone);

        try {
            mp.setDataSource(getActivity().getApplicationContext(), myUri);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            Log.i(HearingVoicesApplication.TAG, e.getMessage());
        }

        return v;
    }

    private void checkPlayingCardsMatch() {
        if (cardsSelectedArray.size() == 2) {
            PlayingCard one = cardArrayList.get(cardsUUIDListArray.indexOf(cardsSelectedArray.get(0)));
            PlayingCard two = cardArrayList.get(cardsUUIDListArray.indexOf(cardsSelectedArray.get(1)));

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            if (one.compareTo(two) == 0) {
                builder.setMessage(R.string.shopping_list_correct_text)
                        .setPositiveButton(R.string.ok_button_text, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                countDownTimer.cancel();
                                mp.release();
                                if (getActivity() != null) {
                                    getActivity().finish();
                                }
                            }
                        });
            } else {
                cardsGridView.getChildAt(cardsUUIDListArray.indexOf(cardsSelectedArray.get(0))).setBackgroundColor(Color.TRANSPARENT);
                cardsGridView.getChildAt(cardsUUIDListArray.indexOf(cardsSelectedArray.get(1))).setBackgroundColor(Color.TRANSPARENT);
                cardsSelectedArray = new ArrayList<String>();

                builder.setMessage(R.string.shopping_list_incorrect_text)
                        .setPositiveButton(R.string.ok_button_text, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
            }
            builder.create().show();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mp.release();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
