package com.footsqueek.android.hearingvoices;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Random;

public class HearingVoicesApplication extends Application {

    static final String TAG = "HearingVoices";
    static final String LIFECYCLETAG = "HearingVoicesLifeCycle";

    public final static String PREFS_NAME = "hearingvoicesprefs";
    public final static String EXTRA_TEXT_STRING = "com.footsqueek.hearingvoices.TEXT_STRING";
    public final static String EXTRA_TEXT_ID = "com.footsqueek.hearingvoices.TEXT_ID";
    public final static String EXTRA_PROMPT_TEXT_ID = "com.footsqueek.hearingvoices.PROMPT_TEXT_ID";
    private static final String decimalFormatStr = "0.00";

    public void onCreate() {

        // Find out if this is the first time
        // the application has been installed

        SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        //firstTimeInstall = settings.getBoolean(String.valueOf(R.string.first_time_install), true);

        // If first time install update the prefs
        // to indicate the new installation

//        if (firstTimeInstall) {
//            SharedPreferences.Editor editor = settings.edit();
//            editor.putBoolean(String.valueOf(R.string.first_time_install), false);
//
//            editor.commit();
//        }

        Log.i(TAG, "Application Loaded");
    }

    public static String stringReaderToString(InputStreamReader sr) {
        BufferedReader r = new BufferedReader(sr);
        StringBuilder total = new StringBuilder();
        String line;

        try {
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            Log.e("TAG", "String Reader Error: " + e.toString());
        }

        return total.toString();
    }

    public static double randomDoubleInRange(double rangeMin, double rangeMax) {

        double randomValue;

        if (Double.valueOf(rangeMax-rangeMin).isInfinite()) {
            return 0;
        } else {
            randomValue = (rangeMin + (Math.random() * rangeMax));
            return randomValue;
        }
    }

    public static int randomIntInRange(int rangeMin, int rangeMax) {
        int randomValue;

        randomValue = (rangeMin + (int)(Math.random() * rangeMax));
        return randomValue;
    }
}