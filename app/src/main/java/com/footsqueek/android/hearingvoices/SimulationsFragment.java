package com.footsqueek.android.hearingvoices;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SimulationsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SimulationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SimulationsFragment extends Fragment {

    private SimulationsGridViewAdapter simulationGridViewAdapter;
    private GridView simulationsGridView;
    private RelativeLayout disclaimerLayout, simulationsLayout;
    private WebView disclaimerWebView;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static SimulationsFragment newInstance() {
        SimulationsFragment fragment = new SimulationsFragment();
        return fragment;
    }

    public SimulationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_simulation, container, false);

        disclaimerLayout = (RelativeLayout) v.findViewById(R.id.disclaimerLayout);
        disclaimerLayout.setVisibility(View.VISIBLE);

        disclaimerWebView = (WebView) v.findViewById(R.id.disclaimerWebView);

        simulationsLayout = (RelativeLayout) v.findViewById(R.id.simulationsLayout);
        simulationsLayout.setVisibility(View.GONE);

        InputStream inputStream = null;
        inputStream = getResources().openRawResource(R.raw.disclaimer);

        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            String htmlRaw = HearingVoicesApplication.stringReaderToString(inputStreamReader);
            String mime = "text/html";
            String encoding = "utf-8";

            Spanned textHtml = Html.fromHtml(HearingVoicesApplication.stringReaderToString(inputStreamReader));

            disclaimerWebView.loadDataWithBaseURL(null, htmlRaw, mime, encoding, null);
            disclaimerWebView.setBackgroundColor(Color.TRANSPARENT);
        }

        Button acceptButton = (Button) v.findViewById(R.id.acceptButton);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disclaimerLayout.setVisibility(View.GONE);
                simulationsLayout.setVisibility(View.VISIBLE);
            }
        });

        Cursor cursor = null;

        // Create a MatrixCursor filled with the rows you want to add.
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{"_id", "title", "summary", "action"});
        matrixCursor.addRow(new Object[]{"1", "Activity 1", "While listening to the audio try and match as many pairs of cars as you can.", 1});
        matrixCursor.addRow(new Object[]{"2", "Activity 2", "While listening to the audio add up the prices of the items on teh shopping list", 2});
        matrixCursor.addRow(new Object[]{"3", "Exercise 1 - 5 minutes minimum", "Listen to this audio recording while having a conversation with one of your colleagues, friends or peers. Perhaps you could discuss a hobby or your most recent holiday.", 3});
        matrixCursor.addRow(new Object[]{"4", "Exercise 2 - 15 - 30 minutes minimum", "Play the audio recording while going about your normal daily activity perhaps do some shopping or take a break in a cafe.", 4});
        matrixCursor.addRow(new Object[]{"5", "Exercise 3 - over an hour", "Experience teh simulated recording for a prolonged period of time, try and continue your normal daily activity for as long as you are able to.", 5});

        // Merge your existing cursor with the matrixCursor you created.
        MergeCursor mergeCursor = new MergeCursor(new Cursor[]{matrixCursor, cursor});

        simulationGridViewAdapter = new SimulationsGridViewAdapter(getActivity(), mergeCursor, true);

        simulationsGridView = (GridView) v.findViewById(R.id.simulationsGridView);
        simulationsGridView.setAdapter(simulationGridViewAdapter);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        simulationsGridView.setVisibility(View.GONE);
//        disclaimerLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        Log.i(HearingVoicesApplication.TAG, "DETACH");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        if (simulationsGridView != null && disclaimerLayout != null) {
            simulationsLayout.setVisibility(View.GONE);
            disclaimerLayout.setVisibility(View.VISIBLE);
        }
    }

}