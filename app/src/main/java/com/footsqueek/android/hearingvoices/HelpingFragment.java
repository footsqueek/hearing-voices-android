package com.footsqueek.android.hearingvoices;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.mobeta.android.dslv.DragSortListView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.footsqueek.android.hearingvoices.HelpingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.footsqueek.android.hearingvoices.HelpingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelpingFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private DragSortListView dragSortListView;
    private HelpingDragSortListViewAdapter helpingDragSortListViewAdapter;
    private ArrayAdapter<String> helpingAdapter;

    private OnFragmentInteractionListener mListener;

    public static HelpingFragment newInstance(int sectionNumber) {
        HelpingFragment fragment = new HelpingFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public HelpingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_helping, container, false);

        Cursor cursor = null;

        // Create a MatrixCursor filled with the rows you want to add.
        MatrixCursor matrixCursor = new MatrixCursor(new String[] { "_id", "title"});

        matrixCursor.addRow(new Object[]{0, "Support for Family and Friends"});
        matrixCursor.addRow(new Object[]{1, "Self Management"});
        matrixCursor.addRow(new Object[]{2, "Medication"});
        matrixCursor.addRow(new Object[]{3, "Community Resources"});
        matrixCursor.addRow(new Object[]{4, "To Be Heard"});
        matrixCursor.addRow(new Object[]{5, "Therapeutic Approaches"});

        // Merge your existing cursor with the matrixCursor you created.
        MergeCursor mergeCursor = new MergeCursor(new Cursor[] { matrixCursor, cursor });

//        helpingDragSortListViewAdapter = new HelpingDragSortListViewAdapter(getActivity(), mergeCursor, true);

        final String[] names = {"Support for Family and Friends", "Self Management", "Medication", "Community Resources", "To Be Heard", "Therapeutic Approaches"};
        final ArrayList<String> list = new ArrayList<String>(Arrays.asList(names));
        final ArrayList<String> listOfNames = new ArrayList<String>(Arrays.asList(names));
        helpingAdapter = new ArrayAdapter<String>(getActivity(), R.layout.helping_list_item, R.id.itemTextView, list);

        dragSortListView = (DragSortListView)v.findViewById(R.id.dragSortListView);
        dragSortListView.setAdapter(helpingAdapter);
        dragSortListView.setDropListener(onDrop);
        dragSortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getApplicationContext(), TextActivity.class);
                int po = listOfNames.indexOf(list.get(position));
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_ID, po + 7);
                startActivity(intent);
            }
        });

        Button exerciseButton = (Button)v.findViewById(R.id.exerciseButton);
        exerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), TextActivity.class);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_ID, 13);
                startActivity(intent);
            }
        });

        return v;
    }

    private DragSortListView.DropListener onDrop = new DragSortListView.DropListener()
    {
        @Override
        public void drop(int from, int to)
        {
            if (from != to)
            {
                String item = helpingAdapter.getItem(from);
                helpingAdapter.remove(item);
                helpingAdapter.insert(item, to);
            }
        }
    };

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
