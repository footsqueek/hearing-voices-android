package com.footsqueek.android.hearingvoices;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by wmaginn on 17/01/2015.
 */
public class  SimulationsGridViewAdapter extends CursorAdapter {

    public SimulationsGridViewAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public SimulationsGridViewAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View simulationItemView = inflater.inflate(R.layout.simulation_list_item, parent, false);

        return simulationItemView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final MediaPlayer mp = new MediaPlayer();

        TextView simulationTitleTextView = (TextView)view.findViewById(R.id.simulationTitleTextView);
        String simulationTitle = cursor.getString(cursor.getColumnIndex("title"));
        simulationTitleTextView.setText(simulationTitle);

        TextView simulationSummaryTextView = (TextView)view.findViewById(R.id.simulationSummaryTextView);
        String simulationSummary = cursor.getString(cursor.getColumnIndex("summary"));
        simulationSummaryTextView.setText(simulationSummary);

        final int actionId = cursor.getInt(cursor.getColumnIndex("action"));
        Toast.makeText(context.getApplicationContext(),"Id: " + actionId, Toast.LENGTH_LONG);

        final Button startSimulationButton = (Button)view.findViewById(R.id.startSimulationButton);
        if (actionId == 3 || actionId == 4 || actionId == 5) {
            startSimulationButton.setText("PLAY");
            startSimulationButton.setSelected(false);
        }
        startSimulationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (actionId) {
                    case 0:

                        break;
                    case 1:
                        intent = new Intent(context.getApplicationContext(), CardMatchingActivity.class);
                        break;
                    case 2:
                        intent = new Intent(context.getApplicationContext(), ShoppingListActivity.class);
                        break;
                }
                if (intent!=null) {
                    context.startActivity(intent);
                } else {
                    Uri myUri = null;

                    switch (actionId) {
                        case 3:
                            myUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.socialoneaudio);
                        break;
                        case 4:
                            myUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.socialtwoaudio);
                        break;
                        case 5:
                            myUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.socialthreeaudio);
                        break;
                    }

                    if (startSimulationButton.isSelected()) {
                        startSimulationButton.setText("PLAY");
                        mp.stop();
                        mp.release();
                    } else {
                        startSimulationButton.setText("STOP");

                        try {
                            mp.setDataSource(context, myUri);
                            mp.prepare();
                            mp.start();
                        } catch (Exception e) {
                            Log.i(HearingVoicesApplication.TAG, e.getMessage());
                        }
                    }
                    startSimulationButton.setSelected(!startSimulationButton.isSelected());
                }
            }
        });

        Button simulationPromptsButton = (Button)view.findViewById(R.id.simulationPromptsButton);
        simulationPromptsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getApplicationContext(), TextActivity.class);
                intent.putExtra(HearingVoicesApplication.EXTRA_PROMPT_TEXT_ID, actionId);
                context.startActivity(intent);
            }
        });
    }
}
