package com.footsqueek.android.hearingvoices;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URI;
import java.text.DecimalFormat;
import java.util.Arrays;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.footsqueek.android.hearingvoices.ShoppingListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.footsqueek.android.hearingvoices.ShoppingListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShoppingListFragment extends Fragment {

    private Button firstButton, secondButton, thirdButton, forthButton, rightAnswerButton;
    private GridView shoppingListGridView;
    private ShoppingListGridViewAdapter shoppingListGridViewAdapter;
    private MediaPlayer mp;

    private OnFragmentInteractionListener mListener;

    // TODO: Rename and change types and number of parameters
    public static ShoppingListFragment newInstance(String param1, String param2) {
        ShoppingListFragment fragment = new ShoppingListFragment();
        return fragment;
    }

    public ShoppingListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_shopping_list, container, false);

        firstButton = (Button)v.findViewById(R.id.firstButton);
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPrice(v);
            }
        });
        secondButton = (Button)v.findViewById(R.id.secondButton);
        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPrice(v);
            }
        });
        thirdButton = (Button)v.findViewById(R.id.thirdButton);
        thirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPrice(v);
            }
        });
        forthButton = (Button)v.findViewById(R.id.forthButton);
        forthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPrice(v);
            }
        });

        Cursor cursor = null;

        double[] priceArray = new double[8];
        String[] itemArray = new String[8];
        String[] itemListArray = {"Eggs", "Sugar", "Olives", "Cheese", "Ham", "Bread", "Milk", "Apples", "Tomatoes", "Oranges", "Carrots"};
        int lengthOfItemList = itemListArray.length - 1;
        double totalPrice = 0;

        for (int i = 0; i < priceArray.length; i++) {
            priceArray[i] = HearingVoicesApplication.randomDoubleInRange(0.00, 2.50);

            String item = itemListArray[HearingVoicesApplication.randomIntInRange(0, lengthOfItemList)];

            while (Arrays.asList(itemArray).contains(item)) {
                item = itemListArray[HearingVoicesApplication.randomIntInRange(0, lengthOfItemList)];
            }
            itemArray[i] = item;
            totalPrice += priceArray[i];
        }

        // Create a MatrixCursor filled with the rows you want to add.
        MatrixCursor matrixCursor = new MatrixCursor(new String[] { "_id", "item", "price"});

        for (int i = 0; i < itemArray.length; i++) {
            matrixCursor.addRow(new Object[]{i + 1, itemArray[i], priceArray[i]});
        }

        DecimalFormat df = new DecimalFormat("0.00");
        totalPrice = Double.valueOf(df.format(totalPrice));

        firstButton.setText("£" + df.format(totalPrice - HearingVoicesApplication.randomDoubleInRange(0.10, 1.00)));
        secondButton.setText("£" + df.format(totalPrice + HearingVoicesApplication.randomDoubleInRange(0.10, 1.00)));
        thirdButton.setText("£" + df.format(totalPrice - HearingVoicesApplication.randomDoubleInRange(0.10, 1.00)));
        forthButton.setText("£" + df.format(totalPrice + HearingVoicesApplication.randomDoubleInRange(0.10, 1.00)));

        int randomAnswerButtonInt = HearingVoicesApplication.randomIntInRange(0,3);
        rightAnswerButton = firstButton;

        switch (randomAnswerButtonInt) {
            case 0:
                rightAnswerButton = firstButton;
                break;
            case 1:
                rightAnswerButton = secondButton;
                break;
            case 2:
                rightAnswerButton = thirdButton;
                break;
            case 3:
                rightAnswerButton = forthButton;
                break;
        }

        rightAnswerButton.setText("£" + df.format(totalPrice));

        // Merge your existing cursor with the matrixCursor you created.
        MergeCursor mergeCursor = new MergeCursor(new Cursor[] { matrixCursor, cursor });

        shoppingListGridViewAdapter = new ShoppingListGridViewAdapter(getActivity(), mergeCursor, true);

        shoppingListGridView = (GridView)v.findViewById(R.id.shoppingListGridView);
        shoppingListGridView.setAdapter(shoppingListGridViewAdapter);

        mp = new MediaPlayer();
        Uri myUri = Uri.parse("android.resource://" + getActivity().getApplicationContext().getPackageName() + "/" + R.raw.activitytwo);

        try {
            mp.setDataSource(getActivity().getApplicationContext(), myUri);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            Log.i(HearingVoicesApplication.TAG, e.getMessage());
        }

        return v;
    }

    private void checkPrice(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (rightAnswerButton.equals(v)) {
            builder.setMessage(R.string.shopping_list_correct_text)
                    .setPositiveButton(R.string.ok_button_text, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mp.release();
                            if (getActivity() != null) {
                                getActivity().finish();
                            }
                        }
                    });
        } else {
            builder.setMessage(R.string.shopping_list_incorrect_text)
                    .setPositiveButton(R.string.ok_button_text, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
        }
        builder.create().show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mp.release();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
