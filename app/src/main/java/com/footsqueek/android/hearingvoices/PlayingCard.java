package com.footsqueek.android.hearingvoices;

import android.net.Uri;

import java.util.Random;
import java.util.UUID;

/**
 * Created by wmaginn on 03/02/2015.
 */
public class PlayingCard implements Comparable<PlayingCard> {
    public int suitId, valueId;
    public String uuid, suit, value;

    private String[] suitArray = {"Clubs", "Diamonds", "Hearts", "Spades"};
    private String[] valueArray = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

    public PlayingCard() {
        this.suitId = HearingVoicesApplication.randomIntInRange(0,3);
        this.valueId = HearingVoicesApplication.randomIntInRange(0,9);
        this.uuid = UUID.randomUUID().toString();
        try {
            this.suit = suitArray[this.suitId];
            this.value = valueArray[this.valueId];
        }catch (Exception ex) {
            this.suitId = 0;
            this.valueId = 0;
            this.suit = suitArray[0];
            this.value = valueArray[0];
        }
    }

    public PlayingCard(int suitId, int valueId) {
        this.suitId = suitId;
        this.valueId = valueId;
        this.uuid = UUID.randomUUID().toString();

        try {
            this.suitId = suitId;
            this.valueId = valueId;
            this.suit = suitArray[suitId];
            this.value = valueArray[valueId];
        } catch (Exception ex) {
            this.suitId = 0;
            this.valueId = 0;
            this.suit = suitArray[0];
            this.value = valueArray[0];
        }
    }

    @Override
    public int compareTo(PlayingCard another) {
        // Returns a negative integer, zero, or a positive integer as this object
        // is less than, equal to, or greater than the specified object.
        if (this.suit.equals(another.suit) && this.value.equals(another.value)) {
            return 0;
        } else {
            return -1;
        }
    }

}
