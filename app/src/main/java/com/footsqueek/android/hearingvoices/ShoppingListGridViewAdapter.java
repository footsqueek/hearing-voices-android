package com.footsqueek.android.hearingvoices;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

/**
 * Created by wmaginn on 17/01/2015.
 */
public class ShoppingListGridViewAdapter extends CursorAdapter {

    public ShoppingListGridViewAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public ShoppingListGridViewAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View shoppingListItemView = inflater.inflate(R.layout.shopping_list_list_item, parent, false);

        return shoppingListItemView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView itemTextView = (TextView)view.findViewById(R.id.itemTextView);
        String itemName = cursor.getString(cursor.getColumnIndex("item"));
        itemTextView.setText(itemName);

        TextView priceTextView = (TextView)view.findViewById(R.id.priceTextView);
        DecimalFormat df = new DecimalFormat("0.00");
        String itemPrice = "£" + df.format(cursor.getDouble(cursor.getColumnIndex("price")));
        priceTextView.setText(itemPrice);
    }
}
