package com.footsqueek.android.hearingvoices;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by wmaginn on 17/01/2015.
 */
public class HelpingDragSortListViewAdapter extends CursorAdapter {

    public HelpingDragSortListViewAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public HelpingDragSortListViewAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View helpingItemView = inflater.inflate(R.layout.helping_list_item, parent, false);

        return helpingItemView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView helpingTextView = (TextView)view.findViewById(R.id.itemTextView);
        String helpingTitle = cursor.getString(cursor.getColumnIndex("title"));
        helpingTextView.setText(helpingTitle);

    }
}
