package com.footsqueek.android.hearingvoices;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TextFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TextFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TextFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private WebView htmlWebView;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */

    public static TextFragment newInstance(int sectionNumber) {
        TextFragment fragment = new TextFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public TextFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_text, container, false);

        htmlWebView = (WebView) view.findViewById(R.id.htmlWebView);
        htmlWebView.setWebChromeClient(new WebChromeClient());
        htmlWebView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && url.startsWith("http://")) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } if (url != null && url.startsWith("https://s3.amazonaws.com/a44343ff55ef-hearing-voices/")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    if (url.endsWith(".mp4")) {
                        intent.setDataAndType(Uri.parse(url), "video/*");
                    } else if (url.endsWith(".mp3")) {
                        intent.setDataAndType(Uri.parse(url), "audio/*");
                    }
                    view.getContext().startActivity(intent);
                    return true;
                } else {
                    return false;
                }
            }
        });

        InputStream inputStream = null;

        int sectionNumber = 0;

        if (getArguments() != null) {
            sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        }

        if (sectionNumber != 0) {
            switch (sectionNumber) {
                case 5:
                    inputStream = getResources().openRawResource(R.raw.podcasts);
                    break;
                case 6:
                    inputStream = getResources().openRawResource(R.raw.resources);
                    break;
            }

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                String htmlRaw = HearingVoicesApplication.stringReaderToString(inputStreamReader);
                String mime = "text/html";
                String encoding = "utf-8";

//                Spanned textHtml = Html.fromHtml(HearingVoicesApplication.stringReaderToString(inputStreamReader));

                htmlWebView.loadDataWithBaseURL(null, htmlRaw, mime, encoding, null);
                htmlWebView.setBackgroundColor(Color.TRANSPARENT);
            }
        } else {
            Intent i = getActivity().getIntent();
            String textString = i.getStringExtra(HearingVoicesApplication.EXTRA_TEXT_STRING);
            int textId = i.getIntExtra(HearingVoicesApplication.EXTRA_TEXT_ID, 0);
            int promptTextId = i.getIntExtra(HearingVoicesApplication.EXTRA_PROMPT_TEXT_ID, 0);

            if (textId != 0) {
                switch (textId) {
                    case 1:
                        getActivity().setTitle("Introduction");
                        inputStream = getResources().openRawResource(R.raw.introduction);
                        break;
                    case 2:
                        getActivity().setTitle("Introduction");
                        inputStream = getResources().openRawResource(R.raw.explanation_introduction);
                        break;
                    case 3:
                        getActivity().setTitle("Social and Cultural");
                        inputStream = getResources().openRawResource(R.raw.explanation_social);
                        break;
                    case 4:
                        getActivity().setTitle("Biological");
                        inputStream = getResources().openRawResource(R.raw.explanation_biological);
                        break;
                    case 5:
                        getActivity().setTitle("Psychological");
                        inputStream = getResources().openRawResource(R.raw.explanation_psychological);
                        break;
                    case 6:
                        getActivity().setTitle("Conclusion");
                        inputStream = getResources().openRawResource(R.raw.explanation_conclusion);
                        break;
                    case 7:
                        getActivity().setTitle("Support for Family and Friends");
                        inputStream = getResources().openRawResource(R.raw.support);
                        break;
                    case 8:
                        getActivity().setTitle("Self Management");
                        inputStream = getResources().openRawResource(R.raw.selfmanagement);
                        break;
                    case 9:
                        getActivity().setTitle("Medication");
                        inputStream = getResources().openRawResource(R.raw.medication);
                        break;
                    case 10:
                        getActivity().setTitle("Community Resources");
                        inputStream = getResources().openRawResource(R.raw.community);
                        break;
                    case 11:
                        getActivity().setTitle("To Be Heard");
                        inputStream = getResources().openRawResource(R.raw.tobeheard);
                        break;
                    case 12:
                        getActivity().setTitle("Therapeutic Approaches");
                        inputStream = getResources().openRawResource(R.raw.therapeutic);
                        break;
                    case 13:
                        getActivity().setTitle("Exercise");
                        inputStream = getResources().openRawResource(R.raw.helpingexersize);
                        break;

                }
            } else if (promptTextId != 0) {
                switch (promptTextId) {
                    case 1:
                        inputStream = getResources().openRawResource(R.raw.ex1prompt);
                        break;
                    case 2:
                        inputStream = getResources().openRawResource(R.raw.ex2prompt);
                        break;
                    case 3:
                        inputStream = getResources().openRawResource(R.raw.ex3prompt);
                        break;
                    case 4:
                        inputStream = getResources().openRawResource(R.raw.ex4prompt);
                        break;
                    case 5:
                        inputStream = getResources().openRawResource(R.raw.ex5prompt);
                        break;

                }
            }

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                String htmlRaw = HearingVoicesApplication.stringReaderToString(inputStreamReader);
                String mime = "text/html";
                String encoding = "utf-8";

                htmlWebView.loadDataWithBaseURL(null, htmlRaw, mime, encoding, null);
                htmlWebView.setBackgroundColor(Color.TRANSPARENT);
            }
        }

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
