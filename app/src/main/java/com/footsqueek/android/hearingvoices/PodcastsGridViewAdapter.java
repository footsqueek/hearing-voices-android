package com.footsqueek.android.hearingvoices;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by wmaginn on 17/01/2015.
 */
public class PodcastsGridViewAdapter extends CursorAdapter {

    public PodcastsGridViewAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public PodcastsGridViewAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View podcastItemView = inflater.inflate(R.layout.podcast_list_item, parent, false);

        return podcastItemView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView podcastTitleTextView = (TextView)view.findViewById(R.id.podcastTitleTextView);
        String simulationTitle = cursor.getString(cursor.getColumnIndex("title"));
        podcastTitleTextView.setText(simulationTitle);

        TextView podcastSummaryTextView = (TextView)view.findViewById(R.id.podcastSummaryTextView);
        String simulationSummary = cursor.getString(cursor.getColumnIndex("summary"));
        podcastSummaryTextView.setText(simulationSummary);
    }
}
