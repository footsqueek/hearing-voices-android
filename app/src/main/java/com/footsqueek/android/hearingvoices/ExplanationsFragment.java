package com.footsqueek.android.hearingvoices;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.footsqueek.android.hearingvoices.R;

import java.io.Serializable;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExplanationsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExplanationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExplanationsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    // TODO: Rename and change types and number of parameters
    public static ExplanationsFragment newInstance(int sectionNumber) {
        ExplanationsFragment fragment = new ExplanationsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ExplanationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_explanations, container, false);

        Button introductionButton = (Button)v.findViewById(R.id.introductionButton);
        introductionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TextActivity.class);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_ID, 2);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_STRING, "Introduction");
                startActivity(intent);
            }
        });

        Button socialButton = (Button)v.findViewById(R.id.socialButton);
        socialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TextActivity.class);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_ID, 3);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_STRING, "Social and Cultural");
                startActivity(intent);
            }
        });

        Button biologicalButton = (Button)v.findViewById(R.id.biologicalButton);
        biologicalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TextActivity.class);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_ID, 4);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_STRING, "Biological");
                startActivity(intent);
            }
        });

        Button psychologicalButton = (Button)v.findViewById(R.id.psychologicalButton);
        psychologicalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TextActivity.class);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_ID, 5);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_STRING, "Psychological");
                startActivity(intent);
            }
        });

        Button conclusionButton = (Button)v.findViewById(R.id.conclusionButton);
        conclusionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TextActivity.class);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_ID, 6);
                intent.putExtra(HearingVoicesApplication.EXTRA_TEXT_STRING, "Conclusion");
                startActivity(intent);
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
